require('newrelic');

var express = require('express');
var compress = require('compression');

var oneDay = 1000 * 60 * 60 * 24 * 30;

var app = express();

app.use(compress({
  threshold : 0, // or whatever you want the lower threshold to be
  filter    : function(req, res) {return true;}

}));

app.use('/', express.static(__dirname + '/game/latest'));
app.use('/compo', express.static(__dirname + '/game/compo', { maxAge: oneDay }));

app.listen(process.env.PORT);