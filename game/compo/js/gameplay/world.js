


function Module(Tile)
{

	return function World(rnd, width, height, tileSize, maxDepth, nbPath, safeZone, hordeProba) {

		this.tiles = [];
		this.tileSize = tileSize;

		// génération du niveau
		this.start = new Tile(0, 0, 'start');
		this.tiles.push(this.start);

		var end = {
			x : (rnd.genrand_int32() % width - width / 2) * 2,
			y : (rnd.genrand_int32() % height - height / 2) * 2
		};

		//this.tiles.push(new Tile(end.x, end.y, 'end'));

		for (var i = 0; i < nbPath; ++i)
			gen(this.tiles, this.start, end, maxDepth, safeZone);

		function gen (list, start, end, maxDepth, safeZone)
		{
			if (maxDepth <= 0)
				return ;

			var x = start.x;
			var y = start.y;

			var diffX = end.x - start.x;
			var diffY = end.y - start.y;
			var max =  Math.max(Math.abs(diffX), Math.abs(diffY));

			var probaX = 1 + (diffX / max);
			var probaY = 1 + (diffY / max);

			var r = rnd.genrand_real1() * 2;

			if (Math.abs(probaX) > Math.abs(probaY))
			{
				if (r < probaX)
					x = start.x + tileSize;
				else if (r > probaX)
					x = start.x - tileSize;
			}
			else
			{
				if (r < probaY)
					y = start.y + tileSize;
				else if (r > probaY)
					y = start.y - tileSize;
			}

			var tile = null;
			for (var i = 0; i < list.length; ++i)
				if (list[i].x == x && list[i].y == y)
				{
					tile = list[i];
					break ;
				}

			if (!tile)
			{
				var type = '';
				if (rnd.genrand_real1() < hordeProba)
					type = 'horde'
				tile = new Tile(x, y, type);
				if (safeZone == 0)
					tile.type = 'start';


				list.push(tile);
				return gen(list, tile, end, maxDepth - 1, safeZone - 1);
			}

			end = {
				x : (rnd.genrand_int32() % width - width / 2) * 2,
				y : (rnd.genrand_int32() % height - height / 2) * 2
			};

			gen(list, start, end, maxDepth - 1, safeZone);

		}


		//this.tiles[this.tiles.length - 1].type = 'end';

		var tile = null;
		var distMax = 0;
		for (var i = 0; i < this.tiles.length; ++i)
		{
			var dist = Math.pow(this.tiles[i].x - this.start.x, 2) + Math.pow(this.tiles[i].y - this.start.y, 2);
			if (dist > distMax && this.tiles[i].type != 'end')
			{
				distMax = dist;
				tile = this.tiles[i];
			}
		}
		tile.type = 'end';




		// var nbTileX = width / tileSize;
		// var y = height / 2 - tileSize / 2;
		// for (var x = 0; x < nbTileX; ++x)
		// 	this.tiles.push(new Tile(x * tileSize, y, ''));

		// // point de spawn
		// this.start = this.tiles[0];
		// this.start.type = 'start';

		// // End
		// this.tiles[this.tiles.length - 1].type = 'end';

		this.GetType = function (x, y)
		{
			for (var i = 0; i < this.tiles.length; ++i)
			{
				var tile = this.tiles[i];
				if (tile.x < x && x <= tile.x + tileSize)
					if (tile.y < y && y <= tile.y + tileSize)
						return tile.type;
			}
			return 'solid';
		}
	};
}


function Test()
{
	var Rnd = require('../external/rnd')
	var gen = new Rnd();

	gen.init_genrand(42)

	var World = new Module(require('./tile'));
	var world = new World(gen, 800, 600, 100, 1);
}


//Test();

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['./tile'], Module);