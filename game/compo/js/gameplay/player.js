

function Module()
{

	return function Player(simulation, zombies, x, y, winTimeTotal, ammo) {
		this.x = x;
		this.y = y;
		this.speed = 150;
		this.health = 1;
		this.radius = 10;
		this.ammo = ammo;
		this.ammoMax = ammo;

		this.callbackHit = null;
		this.callbackEmpty = null;
		this.callbackPick = null;
		this.callbackMove = null;

		Player.Commands = {}
		Player.Commands[Move.name] 		= function (dirH, dirV)	{ return {name: Move.name, 		dirH: dirH, dirV: dirV}}
		Player.Commands[SetSpeed.name] 	= function (speed) 		{ return {name: SetSpeed.name, 	speed: speed}}
		Player.Commands[Shoot.name] 	= function (x, y) 		{ return {name: Shoot.name, 	x: x, y: y}}

		var callbacks = {}
		callbacks[Move.name] 		= Move
		callbacks[SetSpeed.name]	= SetSpeed
		callbacks[Shoot.name]	= Shoot

		var winTime = 0;
		var isSafe = false;
		var isDanger = false;

		function Shoot(cmd)
		{
			if (this.ammo <= 0)
			{
				if (this.callbackEmpty)
					this.callbackEmpty();
				return ;
			}
			--this.ammo;

			var hit = false;
			var crit = false;
			for (var i = 0; i < zombies.length; ++i)
			{
				var zombie = zombies[i];
				var dist = Math.sqrt(Math.pow(zombie.x - cmd.x, 2) + Math.pow(zombie.y - cmd.y, 2));
				var factor = dist / Math.sqrt(Math.pow(zombie.radius, 2));

				if (factor < 0.2)
				{
					factor = 0;
					crit = true;
				}

				if (factor < 1 && zombie.health > 0)
					hit = zombie.Hit(this, -Math.log(factor)/3);
			}

			if (this.callbackHit)
				this.callbackHit(cmd.x, cmd.y, hit, hit && crit);
		}

		function SetSpeed(cmd)
		{
			this.speed = cmd.speed
		}

		function Move(cmd, delta)
		{
			if (!cmd.dirH && !cmd.dirV)
				return
			var len = Math.sqrt(cmd.dirH * cmd.dirH + cmd.dirV * cmd.dirV)

			var newX = this.x + (cmd.dirH / len) * this.speed * delta
			var newY = this.y + (cmd.dirV / len) * this.speed * delta

			if (simulation.world.GetType(newX,newY) == 'solid')
				return

			this.x = newX
			this.y = newY

			if (this.callbackMove)
				this.callbackMove();
		}

		this.Step = function(commands, delta)
		{
			var type = simulation.world.GetType(this.x, this.y)

			isSafe = type == 'start';
			isDanger = type == 'horde';

			if (type == 'end')
				winTime += delta;
			else
				winTime = 0;



			var ammoNeeded = this.ammoMax - this.ammo;

			if (ammoNeeded)
				for (var i = 0; i < simulation.drops.length; ++i)
				{
					var drop = simulation.drops[i];

					var dist = Math.pow(drop.x - this.x, 2) + Math.pow(drop.y - this.y, 2);

					if (dist < Math.pow(drop.radius + this.radius, 2))
					{
						var taken = Math.min(ammoNeeded, drop.quantity);
						this.ammo += taken;
						drop.quantity -= taken;
						ammoNeeded -= taken;

						if (drop.quantity <= 0)
							simulation.drops.splice(i, 1);

						if (this.callbackPick)
							this.callbackPick();
					}
				}




			if (!commands)
				return ;
			for (var i = 0; i < commands.length; ++i)
				callbacks[commands[i].name].call(this, commands[i], delta)
		}

		this.WinPercent = function ()
		{
			var percent = winTime / winTimeTotal
			if (percent > 1)
				percent = 1
			return percent
		}

		this.IsSafe = function ()
		{
			return isSafe;
		}

		this.IsDanger = function ()
		{
			return isDanger;
		}
	}
}


function Test()
{
	var Player = Module()
	var player = new Player(0,0);


	var cmdMove = Player.Commands.Move;
	var cmdSpeed = Player.Commands.SetSpeed;

	player.Step([
		cmdMove(1, 0),
		cmdSpeed(2),
		cmdMove(-1, 0),
		]);

	console.log(player.x == -1, player.y == 0);


}


//Test();


if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([], Module);
