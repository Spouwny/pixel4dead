

function Module()
{

	return function Tile(x, y, type) {
			this.x = x;
			this.y = y;
			this.type = type;
	};
}



if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([], Module);