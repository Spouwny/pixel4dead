


function Module(Player, Zombie, Drop, World, Rnd)
{

	return function Simulation(seed) {

		this.width = 0
		this.height = 0
		this.players = []
		this.zombies = []
		this.drops = []
		this.world = null
		this.rnd = new Rnd()
		this.allWin = false;

		this.rnd.init_genrand(seed)

		this.InitWorld = function (width, height, tileSize, nbPlayers, winTime, maxDepth, nbPath, density, ammo, safeZone, hordeProba, unusualMode)
		{
			this.width = width
			this.height = height

			this.world = new World(this.rnd, width, height, tileSize, maxDepth, nbPath, safeZone, hordeProba)

			for (var i = 0; i < nbPlayers; ++i)
				this.players.push(new Player(this, this.zombies, this.world.start.x + this.world.tileSize / 2, this.world.start.y + this.world.tileSize / 2, winTime, ammo))


			for (var i = 0; i < this.world.tiles.length; ++i)
				if (this.world.tiles[i].type != 'start')
					for (var j = 0; j < density; ++j)
						this.zombies.push(new Zombie(this.rnd, this, this.world.tiles[i].x + this.rnd.genrand_int32() % this.world.tileSize, this.world.tiles[i].y + this.rnd.genrand_int32() % this.world.tileSize, unusualMode))

			for (var i = 0; i < this.world.tiles.length; ++i)
				if (this.world.tiles[i].type == 'start')
					this.Drop(this.world.tiles[i].x + tileSize / 2, this.world.tiles[i].y + tileSize / 2);
		}

		this.Step = function (commands, delta)
		{
			this.allWin = true;
			for (var i = 0; i < this.players.length; ++i)
			{
				this.players[i].Step((commands && commands.players) ? commands.players[i] : undefined, delta)
				if (this.players[i].WinPercent() < 1)
					this.allWin = false;
			}

			for (var i = 0; i < this.zombies.length; ++i)
				this.zombies[i].Step(this.players, delta);
		}

		this.Drop = function(x, y)
		{
			var quantity = this.rnd.genrand_int32() % 3;
			for (var i = 0; i < quantity; ++i)
				this.drops.push(new Drop(x + (this.rnd.genrand_int32() % 20) - 20, y + (this.rnd.genrand_int32() % 20) - 20, 'ammo', 1));
		}

	}
}


function Test()
{
	var Simulation = Module(require('./player'), require('../external/rnd'))
	var simulation = new Simulation(42)
	var simulation2 = new Simulation(42)

	simulation.InitWorld(100, 100, 2)
	simulation2.InitWorld(100, 100, 2)

	console.log('simulation:')
	for (var i = 0; i < simulation.players.length; ++i)
		console.log(simulation.players[i].x, simulation.players[i].y)
	console.log('simulation2:')
	for (var i = 0; i < simulation2.players.length; ++i)
		console.log(simulation2.players[i].x, simulation2.players[i].y)
}

//Test()

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['./player', './zombie', './drop', './world', '../external/rnd'], Module);