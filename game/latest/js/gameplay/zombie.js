

function Module()
{

	return function Zombie(rnd, simulation, x, y, unusualMode) {
		this.x = x;
		this.y = y;
		this.speed = 5;
		this.chaseSpeed = 100;
		this.radius = 10;
		this.headRadius = 10 * 0.3;
		this.health = 1;
		this.damage = 0.5;
		this.chaseDist = 75;
		this.hordeDist = 300;

		this.callbackRush = null;

		var time = 0;
		var state = 'idle';
		var dirH = 0;
		var dirV = 0;
		var timeLimit = 1;
		var currentChaseDist = this.chaseDist;

		this.Hit = function (player, decrement)
		{
			if (this.health <= 0)
				return false;
			this.health -= decrement;
			if (this.health <= 0)
				simulation.Drop(this.x, this.y);

			currentChaseDist =  Math.sqrt(Math.pow(player.x - this.x, 2) + Math.pow(player.y - this.y, 2)) * 2;
			return true;
		}

		this.Step = function(players, delta)
		{
			if (this.health <= 0)
				return ;

			if (currentChaseDist > this.chaseDist)
				currentChaseDist -= 150 * delta;
			else
				currentChaseDist = this.chaseDist;


			var target = null;
			var targetDist = 0;
			for (var i = 0; i < players.length; ++i)
			{
				var player = players[i];

				var maxDist = currentChaseDist;
				if (player.IsDanger())
					maxDist = this.hordeDist;

				var dist = Math.pow(player.x - this.x, 2) + Math.pow(player.y - this.y, 2);

				if (dist < maxDist * maxDist && (targetDist > dist || !targetDist))
				{
					target = player;
					targetDist = dist;
					currentChaseDist = maxDist;
				}

				if (dist < Math.pow(player.radius + this.radius, 2))
					player.health -= this.damage * delta;
			}


			time +=delta;


			if (target)
			{
				if (state != 'chase' && this.callbackRush)
					this.callbackRush();
				state = 'chase';
			}
			else if (state == 'chase')
			{
				time = 0;
				timeLimit = rnd.genrand_real1() * 2;
				state = 'idle';
			}

			switch (state)
			{
				case 'idle':

					if (time > timeLimit)
					{
						time = 0;
						state = 'walk';
						dirH = rnd.genrand_int32() % 3 - 1;
						dirV = rnd.genrand_int32() % 3 - 1;
						timeLimit = rnd.genrand_real1() * 3;
					}
					break ;

				case 'walk':
				{
					var newX = this.x + dirH * this.speed * delta;
					var newY = this.y + dirV * this.speed * delta;

					if (unusualMode)
					{
						var newX = this.x + dirH * this.speed * 10 * delta * Math.sin(time);
						var newY = this.y + dirV * this.speed * 10 * delta * -Math.cos(time);
					}


					if (time > timeLimit)
					{
						time = 0;
						timeLimit = rnd.genrand_real1() * 2;
						state = 'idle';
					}

					if (simulation.world.GetType(newX,newY) == 'solid')
						break ;
					if (simulation.world.GetType(newX,newY) == 'start')
						break ;

					this.x = newX
					this.y = newY
					break;
				}

				case 'chase':
				{
					dirH = target.x - this.x;
					dirV = target.y - this.y;

					var len = Math.sqrt(dirH * dirH + dirV * dirV)

					var newX = this.x + dirH/len * this.chaseSpeed * delta;
					var newY = this.y + dirV/len * this.chaseSpeed * delta;

					if (simulation.world.GetType(newX,newY) == 'solid')
						break ;
					if (simulation.world.GetType(newX,newY) == 'start')
						break ;

					this.x = newX
					this.y = newY

					break;
				}
			}

		}


	}
}


function Test()
{
}


//Test();


if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([], Module);
