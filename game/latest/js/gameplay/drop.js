

function Module()
{

	return function Drop(x, y, type, quantity) {
			this.x = x;
			this.y = y;
			this.quantity = quantity;
			this.radius = 5;
			this.type = type;
	};
}



if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([], Module);