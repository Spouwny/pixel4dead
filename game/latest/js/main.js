
//requirejs(['gameplay/player'], Main);

var Simulation;
var Player;


var game;

var simFrequency = 1/60;
var winTime = 3;

var scale = 1;
var width = 800;
var height = 600;
var tileSize = 150;
var hordeProba = 0.05;

var ambianceTime = 0;
var ambianceBreathTime = 0;
var ambianceSinceLastBreath = 0;

var unusualMode = false;
var nbBulletToLoad = 808;

var detectionZoneTime = 0.3;

function CheckKeys(cmds)
{
	// déplacements

	var x = 0;
	var y = 0;

	if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || game.input.keyboard.isDown(Phaser.Keyboard.Q) || game.input.keyboard.isDown(Phaser.Keyboard.A))
		x = -1;
	if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || game.input.keyboard.isDown(Phaser.Keyboard.D))
		x = 1;
	if (game.input.keyboard.isDown(Phaser.Keyboard.UP) || game.input.keyboard.isDown(Phaser.Keyboard.W) || game.input.keyboard.isDown(Phaser.Keyboard.Z))
		y = -1;
	if (game.input.keyboard.isDown(Phaser.Keyboard.DOWN) || game.input.keyboard.isDown(Phaser.Keyboard.S))
		y = 1;

	if (x || y)
	{
		cmds.push(Player.Commands.Move(x, y));
		if (ambianceSinceLastBreath > 3)
			ambianceBreathTime = 0.5 + Math.random();
	}

}

function _fileComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
    if (success){ this.key = cacheKey;}
}

function dynamicLoadAudio(game, url, fallback, key) {
    if (typeof key === 'undefined') key = 'dynamicLoad_' + url;
    if (game.cache.checkImageKey(key)) {
        return game.add.audio(key);
    } else {
        var audio = game.add.audio(fallback);
        var loader = new Phaser.Loader(game);

        loader.audio(key, url);
        loader.onFileComplete.addOnce(_fileComplete, audio);
        loader.start();

        return audio;
    }
}



var bulletKeys = [];
function _fileImageComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
    if (success){ this.key = cacheKey; bulletKeys.push(cacheKey);}
}

function dynamicLoadBullet(game, url, fallback, key) {
    if (typeof key === 'undefined') key = 'dynamicLoad_' + url;
    if (game.cache.checkImageKey(key)) {
        return game.add.image(key);
    } else {
        var image = game.add.image(fallback);
        var loader = new Phaser.Loader(game);

        loader.image(key, url);
        loader.onFileComplete.addOnce(_fileImageComplete, image);
        loader.start();

        return image;
    }
}

function GameState()
{
	this.preload = function ()
	{

		if (unusualMode)
			this.soundAmbient = dynamicLoadAudio(game, 'assets/organ.mp3', '', 'ambient');
		else
			this.soundAmbient = dynamicLoadAudio(game, 'assets/ambient_0.mp3', '', 'ambient');


		this.soundDeath = dynamicLoadAudio(game, 'assets/death1.ogg', '', 'death');
		// this.soundElevatorDing = dynamicLoadAudio(game, 'assets/elevator_ding.wav', '', 'elevator_ding');

		this.soundFootsteps = [];
		for (var i = 0; i < 10; ++i)
			this.soundFootsteps.push(dynamicLoadAudio(game, 'assets/footsteps_' + i + '.wav', '', 'footsteps_' + i));

		this.soundSqueakSteps = [];
		for (var i = 0; i < 8; ++i)
			this.soundSqueakSteps.push(dynamicLoadAudio(game, 'assets/footstep_metal (' + i + ').wav', '', 'footsteps_metal_' + i));


		this.soundBreaths = [];
		this.lastBreathSound = -1;
		for (var i = 1; i < 3; ++i)
			this.soundBreaths.push(dynamicLoadAudio(game, 'assets/breath_' + i + '.ogg', '', 'breath_' + i));

		this.soundAmbiances = [];
		this.lastAmbientSound = -1;
		for (var i = 1; i < 7; ++i)
			this.soundAmbiances.push(dynamicLoadAudio(game, 'assets/zombie_ambiance_' + i + '.wav', '', 'ambiance_' + i));

		if (unusualMode)
		{
			bulletKeys = [];
			for (var i = 1; i < nbBulletToLoad; ++i)
				dynamicLoadBullet(game, 'assets/Bullets/bullet (' + i + ')', '', 'bullet_' + i);
		}

	}

	this.create = function ()
	{

		this.transition = 'open';

		this.graphicsGround = game.add.graphics(0, 0);

		this.spriteExit = game.add.sprite(0,0, 'exit');
		this.spriteExit.pivot.x = this.spriteExit.width * 0.5;
		this.spriteExit.pivot.y = this.spriteExit.height * 0.5;
		this.spriteExit.width = 64;
		this.spriteExit.height = 64;


		this.graphics = game.add.graphics(0, 0);
		this.difficulty = 0;

		this.difficultyText = game.add.text(5, 10, 'Level ' + this.difficulty);
	    //  Font style
	    this.difficultyText.font = 'Arial';
	    this.difficultyText.fontSize = 15;
	    // text.fontWeight = 'bold';
	    this.difficultyText.fill = '#999999';

		this.ammoText = game.add.text(20, 32, '0 / 0');
	    //  Font style
	    this.ammoText.font = 'Arial';
	    this.ammoText.fontSize = 15;
	    // text.fontWeight = 'bold';
	    this.ammoText.fill = '#999999';

		this.headshotText = game.add.text(5, 10, 'HeadShot !');
	    //  Font style
	    this.headshotText.font = 'Arial';
	    this.headshotText.fontSize = 15;
	    // text.fontWeight = 'bold';
	    this.headshotText.fill = '#FF0000';
	    this.headshotText.alpha = 0;


	    this.emitterHit = game.add.emitter(0, 0, 400);

	    this.emitterHit.makeParticles( [ 'particule_shoot_hit' ] );

	    this.emitterHit.gravity = 0;
	    this.emitterHit.setAlpha(1, 0, 500);
	    this.emitterHit.setScale(0.5, 0, 0.5, 0, 500);
	    if (unusualMode)
	    	this.emitterHit.setScale(0.5, 3, 0.5, 3, 500);
	    this.emitterHit.maxParticles = 100;


	    this.emitterMiss = game.add.emitter(0, 0, 400);

	    this.emitterMiss.makeParticles( [ 'particule_shoot_miss' ] );

	    this.emitterMiss.gravity = 0;
	    this.emitterMiss.setAlpha(1, 0, 500);
	    this.emitterMiss.setScale(0.5, 0, 0.5, 0, 500);
	    if (unusualMode)
	    	this.emitterMiss.setScale(0.5, 3, 0.5, 3, 500);
	    this.emitterMiss.maxParticles = 100;


		this.soundShoot = game.add.audio('shoot');
		this.soundEmpty = game.add.audio('empty');
		this.soundReload = game.add.audio('reload');
		this.soundHit = game.add.audio('hit');
		this.soundRush = game.add.audio('rush');

		this.soundElevatorOpen = game.add.audio('elevator_open');




		this.bulletSprites = []
		for (var i = 0; i < 3; ++i)
		{
			var sprite = game.add.sprite(0,0, '');
			sprite.alpha = 0;
			this.bulletSprites.push(sprite);
		}



		this.graphicsFade = game.add.graphics(0, 0);

		this.graphicsFade.beginFill(0x000000);
		this.graphicsFade.drawRect(0,0,width, height);
		this.graphicsFade.endFill();


		this.pressed = false;


		if (!unusualMode)
		{
			game.input.onDown.add(function() {
				if (this.pressed)
					return ;

				this.pressed = true;
				var center = {
					x: this.oldCenter.x,
					y: this.oldCenter.y,
				};

	  			this.cmds.players[0].push(Player.Commands.Shoot(game.input.x *  1 / scale + center.x, game.input.y * 1 / scale + center.y));

			}, this);


			game.input.onUp.add(function() {
				this.pressed = false;
			}, this)
		}
		else{

			this.bullets = [];
			game.input.onDown.add(function() {
				if (this.pressed || this.bullets.length == this.bulletSprites.length)
					return ;

				if (this.simulation.players[0].ammo <= 0)
				{
					this.soundEmpty.play();
					return ;
				}

				this.pressed = true;
				var center = {
					x: this.oldCenter.x,
					y: this.oldCenter.y,
				};

				this.bullets.push({
					type: bulletKeys[Math.floor(Math.random() * (bulletKeys.length))],
					x: this.simulation.players[0].x,
					y: this.simulation.players[0].y,
					tX: game.input.x *  1 / scale + center.x,
					tY: game.input.y *  1 / scale + center.y
				});



	  			//this.cmds.players[0].push(Player.Commands.Shoot(game.input.x *  1 / scale + center.x, game.input.y * 1 / scale + center.y));

			}, this);


			game.input.onUp.add(function() {
				this.pressed = false;
			}, this)
		}

		this.createLevel();

	}


	this.createLevel = function()
	{
		this.simulation = new Simulation(Math.random() * 100);

		var density =  1 + Math.floor((this.difficulty - 1) / 3);
		if (density > 3)
			density = 3
		var maxDepth = 3 + this.difficulty;
		var ammo = this.difficulty;
		var nbPath = 1 + Math.floor(this.difficulty / 5);

		var safeZone = -1;
		if (nbPath > 1)
			safeZone = Math.floor(maxDepth / 4);

		var hordeProbaDif = hordeProba;
		if (this.difficulty < 2)
			hordeProbaDif = 0;


		if (unusualMode)
			ammo = this.difficulty * 100;



		this.simulation.InitWorld(width + maxDepth * tileSize, height + maxDepth * tileSize, tileSize, 1, winTime, maxDepth, nbPath, density, ammo, safeZone, hordeProbaDif, unusualMode);

		this.time = 0;
		this.cmds = {players : []};

		this.cmds.players[0] = [];


		this.difficultyText.text = 'Level ' + this.difficulty;


		this.oldCenter = {
			x: this.simulation.players[0].x - width / 2 * (1 / scale),
			y: this.simulation.players[0].y - height / 2 * (1 / scale),
			};


		this.detectionZoneTime = {};

		var self = this;

		for (var i = 0; i < this.simulation.zombies.length; ++i)
			this.simulation.zombies[i].callbackRush = function () {
				self.soundRush.play();
			}

		for (var i = 0; i < this.simulation.players.length; ++i)
		{
			var lastTime = 0;
			var lastSound = -1;
			var indexPlayer = i;
			this.simulation.players[i].callbackMove = function () {
				if (game.time.now - lastTime < 500)
					return ;

				var list = (self.simulation.players[indexPlayer].IsDanger()) ? self.soundSqueakSteps : self.soundFootsteps;

				do
				{
					var sound = Math.floor(Math.random() * list.length);
				}
				while (sound == lastSound);
				list[sound].play();
				self.detectionZoneTime[indexPlayer] = detectionZoneTime;
				lastTime = game.time.now;
			};

			this.simulation.players[i].callbackEmpty = function () {
				self.soundEmpty.play();
			};

			this.simulation.players[i].callbackPick = function () {
				self.soundReload.play();
			};

			this.simulation.players[i].callbackHit = function (x, y, hit, crit) {

				var center = {
					x: self.oldCenter.x,
					y: self.oldCenter.y,
				};

				var dirH = self.simulation.players[0].x - x;
				var dirV = self.simulation.players[0].y - y;

				var len = Math.sqrt(dirH * dirH + dirV * dirV)


				var emitter = (hit) ? self.emitterHit : self.emitterMiss;
				emitter.x = (x - center.x) * scale;
  				emitter.y = (y - center.y) * scale;


				var min = 50;
				var max = 200;
				emitter.setXSpeed(-min * dirH / len, -max * dirH / len);
				emitter.setYSpeed(-min * dirV / len, -max * dirV / len);

  				emitter.start(true, 500, 5, 10);

  				self.soundShoot.play();

				if (crit)
				{
					self.headshotText.x = emitter.x - self.headshotText.width / 2;
					self.headshotText.y = emitter.y - self.headshotText.height / 2 - 20;
  					self.headshotText.alpha = 1;
				}
			}
		}
	}

	this.finishLevel = function(death)
	{
		this.transition = 'close';
		this.graphicsFade.clear();
		if (death)
		{
			this.graphicsFade.beginFill(0xFF0000);
			this.graphicsFade.drawRect(0,0,width, height);
			this.soundDeath.play();
		}
		else
		{
			this.graphicsFade.beginFill(0x000000);
			this.graphicsFade.drawRect(0,0,width, height);
			this.soundElevatorOpen.play();
		}
	}

	this.update = function ()
	{
		var deltaTime = game.time.elapsed / 1000;

		if (deltaTime > 0.3)
			deltaTime = 0.3





		switch (this.transition)
		{
			case 'open':
				this.graphicsFade.alpha -= deltaTime * 0.5;
				if (this.graphicsFade.alpha > 0)
					break ;
				this.graphicsFade.alpha = 0;
				this.transition = 'none';
				break ;

			case 'close':
				this.graphicsFade.alpha += deltaTime * 0.5;
				if (this.graphicsFade.alpha < 1)
					return ;
				this.createLevel();
				this.graphicsFade.alpha = 1;
				this.transition = 'open';
				break ;

			case 'none':
				break ;
		}



		this.time += deltaTime;

		if (this.soundAmbient.key != '' && !this.soundAmbient.isPlaying )
			this.soundAmbient.play();


		if (game.input.keyboard.isDown(Phaser.Keyboard.R))
		{
			this.difficulty = 0;
			this.finishLevel(false);
			return ;
		}

		// if (game.input.keyboard.isDown(Phaser.Keyboard.A))
		// {
		// 	this.difficulty += 1;
		// 	this.finishLevel(false);
		// 	return ;
		// }

		// if (game.input.keyboard.isDown(Phaser.Keyboard.Q))
		// {
		// 	this.difficulty -= 1;
		// 	if (this.difficulty < 0)
		// 		this.difficulty = 0;
		// 	this.finishLevel(false);
		// 	return ;
		// }

		// if (game.input.keyboard.isDown(Phaser.Keyboard.Z))
		// 	scale += 0.01;
		// if (game.input.keyboard.isDown(Phaser.Keyboard.S))
		// 	scale -= 0.01;

		// if (game.input.keyboard.isDown(Phaser.Keyboard.E))
		// {
		// 	hordeProba += 0.01;
		// 	this.finishLevel(false);
		// 	return ;
		// }

		// if (game.input.keyboard.isDown(Phaser.Keyboard.D))
		// {
		// 	hordeProba -= 0.01;
		// 	if (hordeProba < 0)
		// 		hordeProba = 0;
		// 	this.finishLevel(false);
		// 	return ;
		// }



		while (this.time > simFrequency)
		{
			CheckKeys(this.cmds.players[0]);
			this.simulation.Step(this.cmds, simFrequency);
			this.time -= simFrequency;
			this.cmds.players[0].length = 0;
		}


		// win
		if (this.simulation.allWin)
		{
			this.difficulty += 1;
			this.finishLevel(false);
		}

		// loose
		for (var i = 0; i < this.simulation.players.length; ++i)
			if (this.simulation.players[i].health < 0)
				this.finishLevel(true);


		// Graphs


		if (this.headshotText.alpha > 0)
			this.headshotText.alpha -= 1 * deltaTime;






		this.graphics.clear();




		var realCenter = {
			x: this.simulation.players[0].x - width / 2 * (1 / scale),
			y: this.simulation.players[0].y - height / 2 * (1 / scale),
			};


		this.oldCenter.x += (realCenter.x - this.oldCenter.x) * 4 * deltaTime;
		this.oldCenter.y += (realCenter.y - this.oldCenter.y) * 4 * deltaTime;

		var center = {
			x: this.oldCenter.x,
			y: this.oldCenter.y,
			};





		function IsInDisplay(x, y, size)
		{
			if ((x - center.x) * scale > width)
				return false;
			if ((y - center.y) * scale > height)
				return false;
			if ((x - center.x) * scale < -size * scale)
				return false;
			if ((y - center.y) * scale < -size * scale)
				return false;
			return true ;
		}

		// World
		this.graphicsGround.clear();
		this.graphicsGround.beginFill(0xFFFFFF);

		var world = this.simulation.world;
		for (var i = 0; i < world.tiles.length; ++i)
		{
			var color = 0xFFFFFF;
			if (world.tiles[i].type == 'start')
				color = 0xDDDDDD;
			if (world.tiles[i].type == 'end')
			{
				color = 0xA9D0F5;
				this.spriteExit.x = (world.tiles[i].x - center.x + world.tileSize / 2) * scale;
				this.spriteExit.y = (world.tiles[i].y - center.y + world.tileSize / 2) * scale;
			}
			if (world.tiles[i].type == 'horde')
				color = 0xFF6666;

			if (!IsInDisplay(world.tiles[i].x, world.tiles[i].y, tileSize))
				continue;

			this.graphicsGround.beginFill(color);
			this.graphicsGround.drawRect ((world.tiles[i].x - center.x) * scale, (world.tiles[i].y - center.y) * scale, world.tileSize * scale, world.tileSize * scale);

		}


		// Drops
		for (var i = 0; i < this.simulation.drops.length; ++i)
		{
			var drop = this.simulation.drops[i];

			if (!IsInDisplay(drop.x, drop.y, drop.radius))
				continue;

			this.graphics.beginFill(0x0000FF);
			this.graphics.drawCircle((drop.x - center.x) * scale,(drop.y - center.y) * scale, drop.radius * 2 * scale);
		}


		// Zombies
		var aliveZombies = 0
		for (var i = 0; i < this.simulation.zombies.length; ++i)
		{
			var zombie = this.simulation.zombies[i];

			if (zombie.health <= 0)
				continue ;

			aliveZombies += 1;

			if (!IsInDisplay(zombie.x, zombie.y, zombie.radius))
				continue;

			// body
			this.graphics.beginFill(0x009900);
			this.graphics.drawCircle((zombie.x - center.x) * scale,(zombie.y - center.y) * scale, zombie.radius * 2 * scale);


			// health
			var color = 0x990000;
			if (unusualMode)
				color = 0xFF00FF;

			this.graphics.beginFill(color);
			this.graphics.drawCircle((zombie.x - center.x) * scale,(zombie.y - center.y) * scale, (1 - zombie.health) * zombie.radius * 2 * scale);


			// head
			this.graphics.beginFill(0x000000);
			this.graphics.drawCircle((zombie.x - center.x) * scale,(zombie.y - center.y) * scale, zombie.headRadius * 2 * scale);



		}


		// Players
		for (var i = 0; i < this.simulation.players.length; ++i)
		{
			var player = this.simulation.players[i];

			if (this.detectionZoneTime[i] && this.detectionZoneTime[i] > 0)
				this.detectionZoneTime[i] -= deltaTime;

			if (!IsInDisplay(player.x, player.y, player.radius))
				continue;

			var color = 0x000000;
			if (unusualMode)
				color = 0xFF00FF;


			if (this.detectionZoneTime[i] && this.detectionZoneTime[i] > 0)
			{
				var percent = (1 - this.detectionZoneTime[i] / detectionZoneTime);
				var maxSize = tileSize / 2;

				if (player.IsDanger())
					maxSize = 300;

				this.graphics.endFill();
				this.graphics.lineStyle(2, 0x999900, 1 - percent * 0.9);
				this.graphics.drawEllipse((player.x - center.x) * scale, (player.y - center.y) * scale, maxSize *percent * scale, maxSize * percent * scale);
			}

			this.graphics.lineStyle(0, 0x000000, 0);
			this.graphics.beginFill(color);
			this.graphics.drawCircle((player.x - center.x) * scale,(player.y - center.y) * scale, player.radius * 2 * scale);

			this.graphics.beginFill(0xFF0000);
			this.graphics.drawCircle((player.x - center.x) * scale, (player.y - center.y) * scale, ((1 - player.health) * player.radius * 2) * scale);

			this.graphics.beginFill(0x9999FF);
			this.graphics.drawCircle((player.x - center.x) * scale, (player.y - center.y) * scale, (player.WinPercent() * player.radius * 2) * scale);




		}



		// bullets

		if (unusualMode)
		{
			for (var i = 0; i < this.bulletSprites.length; ++i)
			{
				var sprite = this.bulletSprites[i];

				if (i >= this.bullets.length)
				{
					sprite.alpha = 0;
					continue;
				}

				var bullet = this.bullets[i];


				var dist = Math.sqrt(Math.pow(bullet.tX - bullet.x, 2) + Math.pow(bullet.tY - bullet.y, 2));

				bullet.x += ((bullet.tX - bullet.x) / dist) * 300 * deltaTime;
				bullet.y += ((bullet.tY - bullet.y) / dist) * 300 * deltaTime;

				var dist2 = Math.sqrt(Math.pow(bullet.tX - bullet.x, 2) + Math.pow(bullet.tY - bullet.y, 2));

				if (dist < dist2)
				{
					this.cmds.players[0].push(Player.Commands.Shoot(bullet.tX, bullet.tY));

					sprite.alpha = 0;

					this.bullets.splice(i, 1);
				}
				else
				{
					sprite.loadTexture(bullet.type);
					sprite.alpha = 1;
					sprite.x = (bullet.x - center.x) * scale;
					sprite.y = (bullet.y - center.y) * scale;
					sprite.rotation = dist / 100;
					sprite.pivot.x = sprite.width / 2;
					sprite.pivot.y = sprite.height / 2;
				}

			};
		}


		// hud

		// for (var i = 0; i < this.simulation.players[0].ammo; ++i)
		// {
			this.graphics.beginFill(0x0000FF);
			this.graphics.drawCircle(10, 40, 10);

			this.ammoText.text = this.simulation.players[0].ammo + ' / ' + this.simulation.players[0].ammoMax;

		// }




		// sounds

		ambianceTime -=  deltaTime;

		if (ambianceTime < 0 && aliveZombies)
		{
			do
			{
				var sound = Math.floor(Math.random() * this.soundAmbiances.length);
			}
			while (sound == this.lastAmbientSound);

			this.lastAmbientSound = sound;
			this.soundAmbiances[sound].play();
			ambianceTime = 5 + Math.random() * 10;
		}





		ambianceBreathTime -=  deltaTime;
		ambianceSinceLastBreath += deltaTime;

		if (ambianceBreathTime < 0)
		{
			do
			{
				var sound = Math.floor(Math.random() * this.soundBreaths.length);
			}
			while (sound == this.lastBreathSound);

			this.lastBreathSound = sound;
			this.soundBreaths[sound].play();
			ambianceBreathTime = 10 + Math.random() * 20;
			ambianceSinceLastBreath = 0;
		}


	}

}


var chooseState = {
	preload: function () {
		},

	create: function () {

			var xLeft = (width / 4);
			var xRight = (width / 4) * 3;
			var y =  height / 2;

			this.validateSound = game.add.audio('shoot');


			this.graphics = game.add.graphics(0, 0);

			this.usualText = game.add.text(0, 0, 'Conventional');
		    //  Font style
		    this.usualText.font = 'Arial';
		    this.usualText.fontSize = 15;
		    // text.fontWeight = 'bold';
		    this.usualText.fill = '#999999';

			this.usualText.x = xLeft - this.usualText.width / 2;
			this.usualText.y = y - this.usualText.height;

			this.unusualText = game.add.text(0, 0, 'Unconventional');
		    //  Font style
		    this.unusualText.font = 'Arial';
		    this.unusualText.fontSize = 15;
		    // text.fontWeight = 'bold';
		    this.unusualText.fill = '#999999';

			this.unusualText.x = xRight - this.unusualText.width / 2;
			this.unusualText.y = y - this.unusualText.height;



			this.headshotText = game.add.text(5, 10, 'HeadShot !');
		    //  Font style
		    this.headshotText.font = 'Arial';
		    this.headshotText.fontSize = 15;
		    // text.fontWeight = 'bold';
		    this.headshotText.fill = '#FF0000';
		    this.headshotText.alpha = 0;



			this.usual = {
				x: xLeft,
				y: y + 20,
				r: 40
			};

			this.unusual = {
				x: xRight,
				y: y + 20,
				r: 40
			};



			this.graphics.beginFill(0x009900);
			this.graphics.drawCircle(this.usual.x, this.usual.y,  this.usual.r);

			this.graphics.beginFill(0x000000);
			this.graphics.drawCircle(this.usual.x, this.usual.y, this.usual.r * 0.3);


			this.graphics.beginFill(0xFF00FF);
			this.graphics.drawCircle(this.unusual.x, this.unusual.y, this.usual.r);

			this.graphics.beginFill(0x000000);
			this.graphics.drawCircle(this.unusual.x, this.unusual.y, this.usual.r * 0.3);




		    this.emitterHit = game.add.emitter(0, 0, 400);

		    this.emitterHit.makeParticles( [ 'particule_shoot_hit' ] );

		    this.emitterHit.gravity = 0;
		    this.emitterHit.setAlpha(1, 0, 500);
		    this.emitterHit.setScale(0.5, 0, 0.5, 0, 500);
		    this.emitterHit.maxParticles = 100;


		    this.emitterMiss = game.add.emitter(0, 0, 400);

		    this.emitterMiss.makeParticles( [ 'particule_shoot_miss' ] );

		    this.emitterMiss.gravity = 0;
		    this.emitterMiss.setAlpha(1, 0, 500);
		    this.emitterMiss.setScale(0.5, 0, 0.5, 0, 500);
		    this.emitterMiss.maxParticles = 100;










			this.pressed = false;
			this.validateUsual = false;
			this.validateUnusual = false;
			this.timeFade = 0;
			game.input.onDown.add(function() {
				if (this.pressed)
					return ;

				this.pressed = true;

				this.validateSound.play();

				var distUsual = Math.sqrt(Math.pow(game.input.x - this.usual.x, 2) + Math.pow(game.input.y - this.usual.y, 2));
				var distUnusual = Math.sqrt(Math.pow(game.input.x - this.unusual.x, 2) + Math.pow(game.input.y - this.unusual.y, 2));


				var emitter = this.emitterMiss;
				if (distUsual < this.usual.r / 2)
				{
					emitter =this.emitterHit;
					this.validate = 'usual';
					this.timeFade = 1;
				}
				if (distUnusual < this.unusual.r / 2)
				{
					emitter =this.emitterHit;
					this.validate = 'unusual';
					this.timeFade = 1;
				}


				if (distUnusual <= this.unusual.r * 0.2 || distUsual <= this.usual.r * 0.2)
				{
					this.headshotText.x = game.input.x - this.headshotText.width / 2;
					this.headshotText.y = game.input.y - this.headshotText.height / 2 - 20;
	  				this.headshotText.alpha = 1;
				}


				emitter.x = game.input.x;
				emitter.y = game.input.y;
				emitter.start(true, 500, 5, 10);

			}, this);


			game.input.onUp.add(function() {
				this.pressed = false;
			}, this)

		},

		update: function () {

		var deltaTime = game.time.elapsed / 1000;

		if (this.headshotText.alpha > 0)
			this.headshotText.alpha -= 1 * deltaTime;


		if (this.timeFade <= 0)
			return ;

		this.timeFade -= deltaTime;

		this.graphics.alpha = this.timeFade;
		this.usualText.alpha = this.timeFade;
		this.unusualText.alpha = this.timeFade;


		if (this.timeFade < 0)
		{
			unusualMode = (this.validate == 'unusual');
			this.game.state.start('game');
		}

		}
};


var preloadState = {
	preload: function () {
			game.preloadBar = game.add.sprite(356, 370, 'preloaderBar');
			game.preloadBar.x = 400 - game.preloadBar.width * 0.5;

			game.load.setPreloadSprite(game.preloadBar);

			game.load.image('particule_shoot_hit', 'assets/particule_shoot_hit.png');
			game.load.image('particule_shoot_miss', 'assets/particule_shoot_miss.png');
			//game.load.image('exit', 'assets/exit.gif');
			game.load.image('exit', 'assets/elevator_big.png');


			game.load.audio('shoot', 'assets/silenced-pistol.wav');
			game.load.audio('empty', 'assets/no-ammo.wav');
			game.load.audio('reload', 'assets/reload.wav');
			game.load.audio('rush', 'assets/rush.wav');
			game.load.audio('elevator_open', 'assets/elevator_close.wav');


		},

	create: function () {
			this.game.state.start('choose');
		}
};


var bootState = {
	preload: function () {
		this.load.image('preloaderBar', 'assets/preloaderBar.png');

		},

	create: function () {

		this.game.state.start('preloadState');


		},

};


function Main(_Simulation, _Player)
{
	Simulation = _Simulation;
	Player = _Player;
	game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas', bootState);
	game.state.add('preloadState', preloadState);
	game.state.add('choose', chooseState);
	game.state.add('game', new GameState());
}




if (typeof define !== 'function') {
		var define = require('amdefine')(module);
}

define(['./gameplay/simulation', './gameplay/player'], Main);
